1.- Clonar el proyecto.
2.- Integrar room como gestor de base de datos.
3.- Crear la entidad ciudad(Nombre, provincia, descripcion, foto/s).
4.- Crear una actividad con un listado de ciudades.
5.- Crear una activddad con el detalle de una ciudad.

- Se valorará la utilización de ViewModels y LiveData
- Se valorará la utilización de Frame
- Se valorará el modelo de programación
- La/s foto/s pueden ser un enlace a una foto pública. En este caso se valorará la forma en la que se carga.

Para incorporar la información lo puedes hacer en la actividad principal o crear una para tal efecto.
